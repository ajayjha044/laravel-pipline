sudo su

#install php and apache2

apt-get install apache2 libapache2-mod-php7.2 mariadb-server php7.2 php7.2-common php7.2-cli php7.2-gd php7.2-mysql php7.2-curl php7.2-intl php7.2-mbstring php7.2-bcmath php7.2-imap php7.2-xml php7.2-zip unzip git curl -y


#install mysql-server 

apt install mysql-server


# Configure MySQL

mysql -u root <<-EOF
CREATE DATABASE homestead;
CREATE USER 'homestead'@'localhost' IDENTIFIED WITH mysql_native_password BY 'secret';
GRANT ALL PRIVILEGES ON databasename.* TO 'homestead'@'localhost';
EOF

cd /var/www/html/
git pull
composer install 
exit 
